using System;
using FlatRedBall;
using Microsoft.Xna.Framework.Graphics;
using BabyTanks.Entities.Gui;
using BabyTanks.Screens;
namespace BabyTanks.Screens
{
	public partial class MainMenu
	{
        void OnExitClick (FlatRedBall.Gui.IWindow callingWindow)
        {
            FlatRedBallServices.Game.Exit();
        }
        void OnServerClick (FlatRedBall.Gui.IWindow callingWindow)
        {
            GlobalData.GameData.NetworkRole = Networking.NetworkAgent.AgentRole.Server;
            this.MoveToScreen(typeof(GameScreen).FullName);
        }
        void OnClientClick (FlatRedBall.Gui.IWindow callingWindow)
        {
            GlobalData.GameData.NetworkRole = Networking.NetworkAgent.AgentRole.Client;
            this.MoveToScreen(typeof(ClientConfig).FullName);
        }
	}
}
