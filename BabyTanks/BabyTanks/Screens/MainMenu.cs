using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Graphics.Model;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
#endif

namespace BabyTanks.Screens
{
	public partial class MainMenu
	{

		void CustomInitialize()
		{
            SetCameraPixelPerfect();
		}

		void CustomActivity(bool firstTimeCalled)
		{
            MoveCursor();

		}

		void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void MoveCursor()
        {
            CursorInstance.X = InputManager.Mouse.WorldXAt(0);
            CursorInstance.Y = InputManager.Mouse.WorldYAt(0);
        }

        public void SetCameraPixelPerfect()
        {
            float yEdge = SpriteManager.Camera.AbsoluteTopYEdgeAt(0);
            float zCam = SpriteManager.Camera.Position.Z;
            float ratio = zCam / yEdge;
            float newZ = Game1.ScreenHeight / 2 * ratio;
            SpriteManager.Camera.Position.Z = newZ;
        }

	}
}
