using System;
using FlatRedBall;
using Microsoft.Xna.Framework.Graphics;
using BabyTanks.Entities.Actors;
using BabyTanks.Entities.Gui;
using BabyTanks.Screens;
using Microsoft.Xna.Framework;
namespace BabyTanks.Screens
{
	public partial class GameScreen
	{
		void OnJoinButtonClick (FlatRedBall.Gui.IWindow callingWindow)
        {
            JoinButton.Visible = false;
            Vector3 position = Vector3.Zero;
            long owner = mNetwork.GetId();
            if (GlobalData.GameData.NetworkRole == Networking.NetworkAgent.AgentRole.Server)
            {
                CreateTank(owner, position);
            }
            else
            {
                SendTankRequestMessage(owner, position);
            }
        }
	}
}
