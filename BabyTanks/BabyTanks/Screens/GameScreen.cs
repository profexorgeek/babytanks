using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Graphics.Model;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
#endif

using System.Collections;
using BabyTanks.Networking;
using BabyTanks.Logging;
using BabyTanks.Input;
using BabyTanks.Entities.Actors;

namespace BabyTanks.Screens
{
	public partial class GameScreen
	{
        List<NetworkMessageIn> mNetworkMessages;
        NetworkAgent mNetwork;
        NetworkMessageOut mInputMessage;
        public ILogger mLogger;
        public enum NetworkMessageType
        {
            InputState,
            CreateTank,
            DestroyTank,
            CreateShot,
            DestroyShot,
            UpdateTank,
            UpdateShot
        }
        InputTypes mCurrentInputState;
        InputTypes mPreviousInputState;
        Vector3 mCurrentMousePosition;
        Tank mOwnedTank;
        float mLastReckoningTime;
        float mAveLatency;

        #region FRB Methods
        void CustomInitialize()
		{
            #if DEBUG
                mLogger = new DebugLogger();
            #else
                mLogger = new ReleaseLogger();
            #endif

            mNetwork = new NetworkAgent(GlobalData.GameData.NetworkRole, "BabyTanks", mLogger);
            if (GlobalData.GameData.NetworkRole == NetworkAgent.AgentRole.Client)
            {
                mNetwork.Connect(GlobalData.GameData.ServerIp);
            }
            mCurrentInputState = InputTypes.None;
            mPreviousInputState = InputTypes.None;
            mLastReckoningTime = 0;
		}
		void CustomActivity(bool firstTimeCalled)
		{
            HandleNetworkActivity();
            HandleCollision();
		}
		void CustomDestroy()
		{
            mNetwork.Shutdown();
		}
        static void CustomLoadStaticContent(string contentManagerName)
        {

        }
        #endregion

        #region Update Activity Methods
        private void HandleNetworkActivity()
        {
            mAveLatency = mNetwork.GetLatency();
            HandleIncomingMessages();
            HandleInput();
            HandleReckoning();
        }
        private void HandleIncomingMessages()
        {
            mNetworkMessages = mNetwork.GetMessages();
            foreach (NetworkMessageIn msg in mNetworkMessages)
            {
                NetworkMessageType type = (NetworkMessageType)msg.ReadByte();
                switch (type)
                {

                    case NetworkMessageType.CreateTank:
                        CreateTank(msg.ReadLong(), msg.ReadVector());
                        break;
                    case NetworkMessageType.UpdateTank:
                        ApplyReckoningTankUpdate(msg);
                        break;
                    case NetworkMessageType.DestroyTank:
                        break;
                    case NetworkMessageType.CreateShot:
                        break;
                    case NetworkMessageType.UpdateShot:
                        break;
                    case NetworkMessageType.DestroyShot:
                        break;
                    case NetworkMessageType.InputState:
                        SetTankInputState(msg.ReadLong(), (InputTypes)msg.ReadByte(), msg.ReadVector());
                        if (GlobalData.GameData.NetworkRole == NetworkAgent.AgentRole.Server)
                        {
                            SendInputUpdateMessage(false);
                        }
                        break;
                }
            }
        }
        private void HandleInput()
        {
            Keyboard kb = InputManager.Keyboard;
            Mouse ms = InputManager.Mouse;
            mCurrentMousePosition = new Vector3(ms.WorldXAt(0), ms.WorldYAt(0), 0);
            mPreviousInputState = mCurrentInputState;
            mCurrentInputState = InputTypes.None;
            mCurrentInputState |= (kb.KeyDown(Keys.W) ? InputTypes.UpPress : InputTypes.None);
            mCurrentInputState |= (kb.KeyDown(Keys.S) ? InputTypes.DownPress : InputTypes.None);
            mCurrentInputState |= (kb.KeyDown(Keys.A) ? InputTypes.LeftPress : InputTypes.None);
            mCurrentInputState |= (kb.KeyDown(Keys.D) ? InputTypes.RightPress : InputTypes.None);
            mCurrentInputState |= (ms.ButtonDown(Mouse.MouseButtons.LeftButton) ? InputTypes.FirePress : InputTypes.None);
            mCurrentInputState |= (ms.ButtonDown(Mouse.MouseButtons.RightButton) ? InputTypes.SpecialPress : InputTypes.None);

            if (mOwnedTank != null)
            {
                mOwnedTank.ChangeInputState(mCurrentInputState, mCurrentMousePosition);
                if (mCurrentInputState != mPreviousInputState)
                {
                    SendInputUpdateMessage(false);
                }
            }
        }
        private void HandleReckoning()
        {
            if (GlobalData.GameData.NetworkRole == NetworkAgent.AgentRole.Server)
            {
                mLastReckoningTime += TimeManager.SecondDifference;
                if (mLastReckoningTime > NetworkForcedSyncFrequency)
                {

                    if (GlobalData.GameData.NetworkRole == NetworkAgent.AgentRole.Server)
                    {
                        foreach (Tank t in Tanks)
                        {
                            SendTankUpdateMessage(t);
                        }
                        // update shots
                        // update other
                    }
                    else
                    {
                        if (mOwnedTank != null)
                        {
                            SendInputUpdateMessage(true);
                        }
                    }

                    mLastReckoningTime = 0;
                }
            }
        }
        private void HandleCollision()
        {
            for (int i = 0; i < Tanks.Count - 1; i++)
            {
                Tank t1 = Tanks[i];
                for (int j = i + 1; j < Tanks.Count; j++)
                {
                    Tank t2 = Tanks[j];
                    t1.Collision.CollideAgainstBounce(t2.Collision, 0.5f, 0.5f, 0.75f);
                }
            }
        }
        #endregion

        #region Message Sending Methods
        private void SendTankRequestMessage(long owner, Vector3 position)
        {
            NetworkMessageOut tankRequest = mNetwork.CreateMessge();
            tankRequest.Write((byte)NetworkMessageType.CreateTank);
            tankRequest.Write(owner);
            tankRequest.Write(position);
            tankRequest.SendToAll(true);
        }
        private void SendTankUpdateMessage(Tank tank) {
            mLogger.FileWrite(String.Format("{0}\t{1}\t{2}\t{3}", tank.Position.X, tank.Position.Y, tank.RotationZ, tank.CurrentSpeed));
            NetworkMessageOut msg = mNetwork.CreateMessge();
            msg.Write((byte)NetworkMessageType.UpdateTank);
            msg.Write(tank.OwnerId);
            msg.Write(tank.Position);
            msg.Write(tank.RotationZ);
            msg.Write(tank.CurrentSpeed);
            msg.Write(tank.CurrentHealth);
            msg.Write((byte)tank.CurrentInputState);
            msg.SendToAll(true);
        }
        private void SendInputUpdateMessage(bool reliable)
        {
            mInputMessage = mNetwork.CreateMessge();
            mInputMessage.Write((byte)NetworkMessageType.InputState);
            mInputMessage.Write(mNetwork.GetId());
            mInputMessage.Write((byte)mCurrentInputState);
            mInputMessage.Write(mCurrentMousePosition);
            mInputMessage.SendToAll(true);
        }
        #endregion

        #region Message Response Methods
        private void SetTankInputState(long owner, InputTypes input, Vector3 targetPoint)
        {
            if (owner != mNetwork.GetId())
            {
                foreach (Tank t in Tanks)
                {
                    if (t.OwnerId == owner)
                    {
                        t.ChangeInputState(input, targetPoint);
                    }
                }
            }
        }
        private void ApplyReckoningTankUpdate(NetworkMessageIn msg) {
            long owner = msg.ReadLong();
            Vector3 position = msg.ReadVector();
            float rotation = msg.ReadFloat();
            float speed = msg.ReadFloat();
            int health = msg.ReadInt();
            InputTypes input = (InputTypes)msg.ReadByte();
            bool tankUpdated = false;
            foreach (Tank t in Tanks) {
                if (t.OwnerId == owner) {
                    if (owner == mNetwork.GetId())
                    {
                        mLogger.FileWrite(String.Format("{0}\t{1}\t{2}\t{3}", position.X, position.Y, rotation, speed));
                        t.ForceNewState(position, rotation, speed, health);
                    }
                    else
                    {
                        t.ForceNewState(position, rotation, speed, health, input);
                    }

                    tankUpdated = true;
                    break;
                }
            }

            // no tank found, create one!
            if (!tankUpdated) {
                CreateTank(owner, position, rotation, speed);
            }
        }
        private void CreateTank(long ownerId, Vector3 position) {
            CreateTank(ownerId, position, 0, 0);
        }
        private void CreateTank(long ownerId, Vector3 position, float rotation, float speed) {
            Tank t = new Tank(ContentManagerName);
            if (ownerId == mNetwork.GetId()) {
                mOwnedTank = t;
            }
            t.OwnerId = ownerId;
            t.Position = position;
            t.CurrentSpeed = speed;
            t.RotationZ = rotation;
            Tanks.Add(t);

            if (GlobalData.GameData.NetworkRole == NetworkAgent.AgentRole.Server) {
                SendTankRequestMessage(ownerId, position);
            }
        }
        #endregion
    }
}
