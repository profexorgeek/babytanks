using System;
using FlatRedBall;
using Microsoft.Xna.Framework.Graphics;
using BabyTanks.Entities.Actors;
using BabyTanks.Entities.Gui;
using BabyTanks.Screens;
namespace BabyTanks.Screens
{
	public partial class ClientConfig
	{
		void OnConnectButtonClick (FlatRedBall.Gui.IWindow callingWindow)
        {
            GlobalData.GameData.ServerIp = ServerIpInput.InputDisplayText;
            this.MoveToScreen(typeof(GameScreen).FullName);
        }

	}
}
