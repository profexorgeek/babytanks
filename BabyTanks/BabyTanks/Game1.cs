using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Utilities;

using BabyTanks.Screens;
using BabyTanks.GlobalData;

namespace BabyTanks
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        GraphicsOptions graphicOptions;

        public const int ScreenWidth = 800;
        public const int ScreenHeight = 600;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferMultiSampling = false;
            Content.RootDirectory = "Content";
			BackStack<string> bs = new BackStack<string>();
			bs.Current = string.Empty;
        }

        protected override void Initialize()
        {
            //graphicOptions = new GraphicsOptions();
            //graphicOptions.SuspendDeviceReset();
            //graphicOptions.UseMultiSampling = false;
            //graphicOptions.TextureFilter = TextureFilter.Point;
            //graphicOptions.SetResolution(ScreenWidth, ScreenHeight);
            //graphicOptions.ResumeDeviceReset();
            Renderer.UseRenderTargets = false;
            FlatRedBallServices.InitializeFlatRedBall(this, graphics);
			GlobalContent.Initialize();

            SetDefaultColors();
			Screens.ScreenManager.Start(typeof(BabyTanks.Screens.MainMenu).FullName);
            base.Initialize();
        }


        protected override void Update(GameTime gameTime)
        {
            FlatRedBallServices.Update(gameTime);
            ScreenManager.Activity();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            FlatRedBallServices.Draw();
            base.Draw(gameTime);
        }

        private void SetDefaultColors()
        {
            FlatRedBallServices.GraphicsOptions.BackgroundColor = Color.White;
            FlatRedBall.Debugging.Debugger.TextBlue = 0;
            FlatRedBall.Debugging.Debugger.TextGreen = 0;
            FlatRedBall.Debugging.Debugger.TextRed = 0;
        }

        
    }
}
