﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BabyTanks.Networking;

namespace BabyTanks.GlobalData
{
    public static class GameData
    {
        private static NetworkAgent.AgentRole mNetworkRole;
        public static NetworkAgent.AgentRole NetworkRole
        {
            get
            {
                if (mNetworkRole == null)
                {
                    mNetworkRole = NetworkAgent.AgentRole.Server;
                }
                return mNetworkRole;
            }
            set
            {
                mNetworkRole = value;
            }
        }
        public static string ServerIp { get; set; }
        public const int PositionTolerance = 4;
        public const float InterpolationRate = 0.05f;



        public static void LoadTanks() {

        }
    }
}
