﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace BabyTanks.Networking
{
    public class NetworkMessageIn
    {
        NetPeer mNetPeer;
        NetIncomingMessage mMessage;

        public NetworkMessageIn(NetPeer peer, NetIncomingMessage msg)
        {
            mNetPeer = peer;
            mMessage = msg;
        }

        public void Recycle()
        {
            mNetPeer.Recycle(mMessage);
        }

        public byte ReadByte()
        {
            return mMessage.ReadByte();
        }
        public float ReadFloat()
        {
            return mMessage.ReadFloat();
        }
        public int ReadInt()
        {
            return mMessage.ReadInt32();
        }
        public long ReadLong()
        {
            return mMessage.ReadInt64();
        }
        public Vector3 ReadVector()
        {
            Vector3 vector = new Vector3();
            vector.X = mMessage.ReadFloat();
            vector.Y = mMessage.ReadFloat();
            vector.Z = mMessage.ReadFloat();
            return vector;
        }
        

    }
}
