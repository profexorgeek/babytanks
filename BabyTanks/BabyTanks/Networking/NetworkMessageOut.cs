﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Lidgren.Network;

using BabyTanks.Logging;
using Microsoft.Xna.Framework;

namespace BabyTanks.Networking
{
    public class NetworkMessageOut
    {
        private NetPeer mPeer;
        private NetOutgoingMessage mMessage;

        public NetworkMessageOut(NetPeer peer)
        {
            if (peer == null)
            {
                throw new SystemException("Failed to create message: bad NetworkPeer.");
            }

            mPeer = peer;
            mMessage = mPeer.CreateMessage();
        }

        public void Send(NetConnection recipient)
        {
            Send(recipient, false);
        }
        public void Send(NetConnection recipient, bool IsGuaranteed)
        {
            NetDeliveryMethod method = IsGuaranteed ? NetDeliveryMethod.ReliableOrdered : NetDeliveryMethod.UnreliableSequenced;
            mPeer.SendMessage(mMessage, recipient, method);
            mMessage = mPeer.CreateMessage();
        }

        public void SendToAll(bool IsGuaranteed)
        {
            NetDeliveryMethod method = IsGuaranteed ? NetDeliveryMethod.ReliableOrdered : NetDeliveryMethod.UnreliableSequenced;
            if (mPeer.Connections.Count > 0)
            {
                mPeer.SendMessage(mMessage, mPeer.Connections, method, 0);
            }
        }

        public long GetUniqueId()
        {
            return mPeer.UniqueIdentifier;
        }

        public void Write(string message)
        {
            mMessage.Write(message);
        }
        public void Write(bool message)
        {
            mMessage.Write(message);
        }
        public void Write(byte message)
        {
            mMessage.Write(message);
        }
        public void Write(Int16 message)
        {
            mMessage.Write(message);
        }
        public void Write(Int32 message)
        {
            mMessage.Write(message);
        }
        public void Write(Int64 message)
        {
            mMessage.Write(message);
        }
        public void Write(float message)
        {
            mMessage.Write(message);
        }
        public void Write(double message)
        {
            mMessage.Write(message);
        }
        public void Write(Vector3 vector)
        {
            mMessage.Write(vector.X);
            mMessage.Write(vector.Y);
            mMessage.Write(vector.Z);
        }
    }
}
