﻿using System;
using System.Collections.Generic;

// Requires the gen3 Lidgren.Network available here:
// http://code.google.com/p/lidgren-network-gen3/
using Lidgren.Network;
using BabyTanks.Logging;

namespace BabyTanks.Networking
{
    public class NetworkAgent
    {
        protected NetPeer mPeer;
        protected NetPeerConfiguration mConfig;
        protected List<NetworkMessageIn> mIncomingMessages;
        protected AgentRole mRole;
        private Random mRandom;
        public ILogger mLogger;
        private const int DefaultPort = 14242;
        public bool IsAuthority { get; set; }
        public enum AgentRole
        {
            Client,
            Server
        }

        public NetworkAgent(AgentRole role, string tag, ILogger logger)
        {
            mRole = role;
            mConfig = new NetPeerConfiguration(tag);

#if DEBUG
            //// simulate packet loss for debugging
            //mConfig.SimulatedLoss = 0.05f;
            //// simulate lag
            //mConfig.SimulatedRandomLatency = 0.1f;
#endif

            mLogger = logger;
            mRandom = new Random();

            if (mRole == AgentRole.Server)
            {
                mConfig.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
                mConfig.Port = DefaultPort;
                mPeer = new NetServer(mConfig);
                IsAuthority = true;
            }
            if (mRole == AgentRole.Client)
            {
                mConfig.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
                mPeer = new NetClient(mConfig);
                IsAuthority = false;
            }

            mPeer.Start();
            mIncomingMessages = new List<NetworkMessageIn>();

            mLogger.Write(String.Format("{0} peer #{1} ready.", mRole.ToString(), GetId()));
        }

        public long GetId()
        {
            return mPeer.UniqueIdentifier;
        }

        public float GetLatency()
        {
            if (mPeer.ConnectionsCount == 0)
            {
                return 0f;
            }

            float latency = 0;
            foreach (NetConnection c in mPeer.Connections)
            {
                latency += c.AverageRoundtripTime;
            }
            latency = latency / (float)mPeer.ConnectionsCount;
            return latency;
        }

        public void Connect(string ip)
        {
            if (mRole == AgentRole.Client)
            {
                mPeer.Connect(ip, DefaultPort);
                mLogger.Write(String.Format("Connecting to: {0}:{1}", ip, DefaultPort));
            }
            else
            {
                string msg = "Exception: Attempted to connect as server. Only clients should connect.";
                mLogger.Write(msg);
                throw new SystemException(msg);
            }
        }

        public NetworkMessageOut CreateMessge()
        {
            NetworkMessageOut msg = new NetworkMessageOut(mPeer);
            return msg;
        }

        public void Shutdown()
        {
            mPeer.Shutdown("Closing connection.");
        }

        public List<NetworkMessageIn> GetMessages()
        {
            mIncomingMessages.Clear();
            NetIncomingMessage msg;
            while ((msg = mPeer.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DiscoveryRequest:
                        mLogger.Write("Discovery request: " + msg.SenderEndpoint);
                        mPeer.SendDiscoveryResponse(null, msg.SenderEndpoint);
                        break;
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                        mLogger.Write(String.Format("Debug Message: {0}", msg.ReadString()));
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        mLogger.Write(String.Format("Status Message: {0}", msg.ReadString()));
                        break;
                    case NetIncomingMessageType.Data:
                        mIncomingMessages.Add(new NetworkMessageIn(mPeer, msg));
                        break;
                    default:
                        // unknown message type
                        break;
                }
            }
            return mIncomingMessages;
        }
    }
}
