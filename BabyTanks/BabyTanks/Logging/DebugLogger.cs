﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlatRedBall.Debugging;
using System.IO;
using System.Timers;


namespace BabyTanks.Logging
{
    public class DebugLogger : ILogger
    {
        Stream mFileStream;
        string mFileName;

        public DebugLogger() {
            mFileName = String.Format("{0}/{1}_log.txt", System.Environment.CurrentDirectory, GlobalData.GameData.NetworkRole.ToString());
            
        }
        
        public void Write(string message)
        {
            Console.WriteLine(message);
            Debugger.CommandLineWrite(message);

            
        }

        public void Write(object obj)
        {
            Write(obj.ToString());
        }

        public void FileWrite(string message)
        {
            try
            {
                message = String.Format("{0}\t{1}\n", DateTime.Now.ToLongTimeString(), message);
                using (mFileStream = File.Open(mFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter writer = new StreamWriter(mFileStream))
                    {
                        writer.Write(message);
                    }
                }
            }
            catch
            {
                // do nothing. this is thrown when two clients on the same machine access the same file.
            }
        }

        public void FileWrite(object obj)
        {
            FileWrite(obj.ToString());
        }
    }
}
