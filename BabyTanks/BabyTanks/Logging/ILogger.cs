﻿namespace BabyTanks.Logging
{
    public interface ILogger
    {
        void Write(string message);
        void Write(object obj);
        void FileWrite(string message);
        void FileWrite(object obj);
    }
}
