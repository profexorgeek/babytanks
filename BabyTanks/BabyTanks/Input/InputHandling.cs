﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BabyTanks.Input
{
    [Flags]
    public enum InputTypes
    {
        None = 0,
        UpPress = 1,
        DownPress = 2,
        LeftPress = 4,
        RightPress = 8,
        FirePress = 16,
        SpecialPress = 32
    }

    public static class InputTypeExtensions
    {
        public static bool ContainsFlag(this InputTypes flagSource, InputTypes testFlag)
        {
            return ((flagSource & testFlag) == testFlag);
        }
    }
}
