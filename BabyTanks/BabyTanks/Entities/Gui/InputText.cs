using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
#endif

namespace BabyTanks.Entities.Gui
{
	public partial class InputText
	{

        bool mIsEditing;

		private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{
            HandleTextInput();
		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        void HandleTextInput()
        {
            if (!mIsEditing && HasCursorOver(GuiManager.Cursor) && InputManager.Mouse.ButtonPushed(Mouse.MouseButtons.LeftButton))
            {
                mIsEditing = true;
            }

            if (mIsEditing)
            {
                if (InputManager.Keyboard.KeyPushed(Keys.Enter))
                {
                    mIsEditing = false;
                }
                else if (InputManager.Keyboard.KeyPushed(Keys.Back))
                {
                    string inputText = InputArea.DisplayText;
                    if (inputText.Length > 0)
                    {
                        InputArea.DisplayText = inputText.Substring(0, inputText.Length - 1);
                    }
                }
                else
                {
                    InputArea.DisplayText += InputManager.Keyboard.GetStringTyped();
                }
                                
            }

            

        }
	}
}
