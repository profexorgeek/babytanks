using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
#endif

using BabyTanks.Input;
using BabyTanks.Logging;

namespace BabyTanks.Entities.Actors
{
	public partial class Tank
	{
        public InputTypes CurrentInputState {get; private set;}
        Vector3 mTarget;

        // reckoning interpolation
        Vector3 mPositionDelta;

		private void CustomInitialize()
		{
            mPositionDelta = new Vector3();
            Collision.Visible = false;
		}

		private void CustomActivity()
		{
            ApplyInputState();
		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {
            
        }

        public void ChangeInputState(InputTypes newState, Vector3 newTarget) {
            CurrentInputState = newState;
            mTarget = newTarget;
        }

        public void ApplyInputState()
        {
            bool emit = false;

            if (CurrentInputState.ContainsFlag(InputTypes.UpPress))
            {
                CurrentSpeed += AccelRate;
                emit = true;
            }

            if (CurrentInputState.ContainsFlag(InputTypes.DownPress))
            {
                CurrentSpeed -= AccelRate;
                emit = true;
            }

            if (CurrentInputState.ContainsFlag(InputTypes.LeftPress))
            {
                RotationZ += TurnRate;
                emit = true;
            }

            if (CurrentInputState.ContainsFlag(InputTypes.RightPress))
            {
                RotationZ -= TurnRate;
                emit = true;
            }

            CurrentSpeed = (CurrentSpeed > MaxSpeed) ? MaxSpeed : CurrentSpeed;
            CurrentSpeed = (CurrentSpeed < -MaxSpeed) ? -MaxSpeed : CurrentSpeed;
            if (CurrentSpeed > 0) {
                CurrentSpeed -= AccelRate / 2;
                emit = true;
            }
            if (CurrentSpeed < 0) {
                CurrentSpeed += AccelRate / 2;
                emit = true;
            }

            Velocity.X = (float)(Math.Cos(RotationZ) * CurrentSpeed);
            Velocity.Y = (float)(Math.Sin(RotationZ) * CurrentSpeed);

            TankTurret.RelativeRotationZ = (float)Math.Atan2(mTarget.Y - Position.Y, mTarget.X - Position.X) - RotationZ;
            TankBarrel.RelativeRotationZ = TankTurret.RelativeRotationZ;

            if (emit)
            {
                TrackEmitter.TimedEmit();
            }

            // apply interpolation to Forced State
            InterpolateReckoningDelta();
        }

        public void ForceNewState(Vector3 position, float rotation, float speed, int health)
        {
            ForceNewState(position, rotation, speed, health, CurrentInputState);
        }
        public void ForceNewState(Vector3 position, float rotation, float speed, int health, InputTypes input)
        {
            mPositionDelta = Vector3.Subtract(position, Position);
            RotationZ = rotation;
            CurrentInputState = input;
            CurrentSpeed = speed;
            CurrentHealth = health;
        }

        private void InterpolateReckoningDelta()
        {
            if (mPositionDelta.X != 0)
            {
                Position.X += mPositionDelta.X * GlobalData.GameData.InterpolationRate;
                mPositionDelta.X -= mPositionDelta.X * GlobalData.GameData.InterpolationRate;
            }

            if (mPositionDelta.Y != 0)
            {
                Position.Y += mPositionDelta.Y * GlobalData.GameData.InterpolationRate;
                mPositionDelta.Y -= mPositionDelta.Y * GlobalData.GameData.InterpolationRate;
            }
        }
	}
}
